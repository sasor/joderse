package edu.umss.fcyt.tourismapp.paquete_turistico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
public class PaqueteTuristicoController {

    @Autowired
    private PaqueteTuristicoService paqueteTuristicoService;
    @Autowired
    private FotoService fotoService;

    @GetMapping("/paquetes-turisticos")
    public List<PaqueteTuristico> index() {
        return paqueteTuristicoService.index();
    }

    @PostMapping("/paquete-turistico")
    public PaqueteTuristico save(@Valid @RequestBody PaqueteTuristico paqueteTuristico) {
        //Foto foto_guardada = fotoService.save();
        //paqueteTuristico.getFotos().add(foto_guardada);
        return paqueteTuristicoService.save(paqueteTuristico);
    }

    @GetMapping("/paquete-turistico/{id}")
    public PaqueteTuristico show(@PathVariable(value = "id") Long id) {
        return paqueteTuristicoService.getOne(id);
    }
}
