package edu.umss.fcyt.tourismapp;

import edu.umss.fcyt.tourismapp.paquete_turistico.PaqueteTuristicoRepository;
import edu.umss.fcyt.tourismapp.paquete_turistico.PaqueteTuristicoService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import edu.umss.fcyt.tourismapp.paquete_turistico.Foto;
import edu.umss.fcyt.tourismapp.paquete_turistico.PaqueteTuristico;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@SpringBootApplication
public class TourismAppApplication {

    @Autowired
    private PaqueteTuristicoService paqueteTuristicoService;

	public static void main(String[] args) {
		SpringApplication.run(TourismAppApplication.class, args);
	}

    @Bean
    InitializingBean sendDatabase() {
        return () -> {
              PaqueteTuristico pt = new PaqueteTuristico();
              pt.setPrecio(new BigDecimal("234.12"));
              pt.setNombre("pt");
              pt.setDescripcion("descripcion");

              Foto f = new Foto();
              f.setTipo("image/png");
              f.setNombre("fotini");

              pt.getFotos().add(f);
              paqueteTuristicoService.save(pt);
        };
    }
}
