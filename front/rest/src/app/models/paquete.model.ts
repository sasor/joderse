import {Foto} from "./foto.model";

export class Paquete {
  id: number;
  nombre: string;
  precio: number;
  descripcion: string;
  fotos: Foto[];
}
