import { Component, OnInit } from '@angular/core';
import { Paquete } from '../models/paquete.model';
import { Foto } from '../models/foto.model';
import { PaqueteService } from '../services/paquete.service';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit {

  submitted = false;
  paquetes: Paquete[];

  model: Paquete = new Paquete();
  fotos: Foto[] = [];

  constructor(private service: PaqueteService) {}

  ngOnInit() {
    //this.paquetes = this.service.getData();
    console.log(this.paquetes);
  }

  onSubmit() {
    this.submitted = true;
    this.model.fotos = this.fotos;
    console.log('hey mandar back');
    console.log(this.model);
    this.service.saveData(this.model).subscribe(paquete => console.log(paquete));
  }

  onFileChange(e) {

    if (e.target.files && e.target.files.length > 0) {

      const files = e.target.files;
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = () => {
          console.log('se agrego correctamente');
          let foto: Foto = new Foto();
          foto.nombre = file.name;
          foto.tipo = file.type;
          foto.data = reader.result;

          this.fotos.push(foto);
        };
        reader.readAsDataURL(file);
      }
    }
  }

  get diagnostic() { return JSON.stringify(this.model); }

}
