import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Paquete } from '../models/paquete.model';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PaqueteService {

  constructor(private http: HttpClient) {

  }

  getData() {
    return [
      {
        nombre: "Paquete #1",
        precio: 234.12,
        descripcion: "la descripcion completa de este PT",
        fotos: [
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          }
        ]
      },
      {
        nombre: "Paquete #1",
        precio: 234.12,
        descripcion: "la descripcion completa de este PT",
        fotos: [
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          }
        ]
      },
      {
        nombre: "Paquete #1",
        precio: 234.12,
        descripcion: "la descripcion completa de este PT",
        fotos: [
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          }
        ]
      },
      {
        nombre: "Paquete #1",
        precio: 234.12,
        descripcion: "la descripcion completa de este PT",
        fotos: [
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          },
          {
            nombre: "foto nombre",
            tipo: "image/png",
            data: "fulldata image"
          }
        ]
      }
    ];
  }

  saveData(paquete: Paquete): Observable<Paquete> {
    return this.http.post<Paquete>('http://localhost:8080/paquete-turistico', paquete, httpOptions);
  }
}
